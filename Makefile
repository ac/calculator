# SPDX-FileCopyrightText: 2020 Antonino Catinello
# SPDX-License-Identifier: MIT

calc: stack.o calculator.c license.o
	$(CC) -Wall -Wextra -Werror -DVERSION=\"$(version)\" license.o -o calc calculator.c stack.c -lm

stack_test: stack.o stack_test.c
	$(CC) -o stack_test stack_test.c stack.c

stack.o: stack.c stack.h
	$(CC) -c stack.c

license.o: license.s license.h
	$(CC) -c license.s

clean:
	rm -f *.o calc stack_test

.PHONY: calc
