// SPDX-FileCopyrightText: 2020 Antonino Catinello, 2012-2016 Brandon Mills
// SPDX-License-Identifier: MIT

#if defined(__linux__)
#define _DEFAULT_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <getopt.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <ctype.h>
#include <libgen.h>
#include <err.h>

#include "stack.h"
#include "license.h"
#include "help.h"
#include "result.h"
#include "define.h"

#define MAXTOKENLENGTH 512
#define MAXPRECISION 15
#define DEFAULTPRECISION -1
#define FUNCTIONSEPARATOR "|"

#ifndef NAN
#define NAN (0.0/0.0)
#endif

#ifndef INFINITY
#define INFINITY (1.0/0.0)
#endif

typedef enum {
    addop,
    multop,
    expop,
    lparen,
    rparen,
    digit,
    value,
    decimal,
    space,
    text,
    function,
    identifier,
    argsep,
    invalid
} Symbol;

static struct Preferences {
    struct Display {
        bool tokens;
        bool postfix;
    } display;
    struct Mode {
        bool degrees;
    } mode;
    int precision;
    unsigned int maxtokenlength;
} prefs;

typedef enum {
    divZero,
    overflow,
    parenMismatch,
    inputMissing,
} Error;

typedef char *token;

typedef double number;

static void enhance(Error err) {
    char *msg;
    msg = "";

    switch (err) {
        case divZero:
            msg = "Divide by zero";
            break;

        case overflow:
            msg = "Overflow";
            break;

        case parenMismatch:
            msg = "Mismatched parentheses";
            break;

        case inputMissing:
            msg = "Function input missing";
            break;
    }

    fprintf(stderr, "Error: %s\n", msg);
}

inline unsigned int toDigit(char ch) {
    return (unsigned int)ch - '0';
}

static number buildNumber(token str) {
    number result = 0;
    result = strtod(str, NULL);
    return result;
}

static token num2Str(number num) {
    int len = 0;
    int counter = 0;
    int precision = MAXPRECISION;

    if (prefs.precision >= 0 && prefs.precision < precision)
        precision = prefs.precision;

    token str = (token)malloc(prefs.maxtokenlength * sizeof(char));
    len = snprintf(str, prefs.maxtokenlength - 1, "%.*f", precision, num);

    if (prefs.precision == -1) {
        while (str[len - 1] == '0') {
            len = snprintf(str, prefs.maxtokenlength - 1, "%.*f", --precision, num);
            counter++;

            if (counter == MAXPRECISION)
                break;
        }
    }

    return str;
}

static number toRadians(number degrees) {
    return degrees * M_PI / 180.0;
}

static number toDegrees(number radians) {
    return radians * 180.0 / M_PI;
}

static int doFunc(Stack *s, token function) {
    if (stackSize(s) == 0) {
        enhance(inputMissing);
        stackPush(s, num2Str((number)NAN));
        return -1;
    } else if (stackSize(s) == 1 && strcmp(stackTop(s), FUNCTIONSEPARATOR) == 0) {
        stackPop(s);
        enhance(inputMissing);
        stackPush(s, num2Str((number)NAN));
        return -1;
    }

    token input = (token)stackPop(s);
    number num = buildNumber(input);
    number result = num;
    number counter = 0;

    if (strncmp(function, "abs", 3) == 0)
        result = fabs(num);
    else if (strncmp(function, "floor", 5) == 0)
        result = floor(num);
    else if (strncmp(function, "ceil", 4) == 0)
        result = ceil(num);
    else if (strncmp(function, "sin", 3) == 0)
        result = !prefs.mode.degrees ? sin(num) : sin(toRadians(num));
    else if (strncmp(function, "cos", 3) == 0)
        result = !prefs.mode.degrees ? cos(num) : cos(toRadians(num));
    else if (strncmp(function, "tan", 3) == 0)
        result = !prefs.mode.degrees ? tan(num) : tan(toRadians(num));
    else if (strncmp(function, "arcsin", 6) == 0
             || strncmp(function, "asin", 4) == 0)
        result = !prefs.mode.degrees ? asin(num) : toDegrees(asin(num));
    else if (strncmp(function, "arccos", 6) == 0
             || strncmp(function, "acos", 4) == 0)
        result = !prefs.mode.degrees ? acos(num) : toDegrees(acos(num));
    else if (strncmp(function, "arctan", 6) == 0
             || strncmp(function, "atan", 4) == 0)
        result = !prefs.mode.degrees ? atan(num) : toDegrees(atan(num));
    else if (strncmp(function, "sqrt", 4) == 0)
        result = sqrt(num);
    else if (strncmp(function, "cbrt", 4) == 0)
        result = cbrt(num);
    else if (strncmp(function, "log", 3) == 0)
        result = log(num);
    else if (strncmp(function, "exp", 3) == 0)
        result = exp(num);
    else if (strncmp(function, "round", 5) == 0)
        result = round(num);
    else if (strncmp(function, "fibs", 4) == 0) {
        int n = (int)num;
        result = 0;

        if (n <= 0)
            result = (number)NAN;
        else if (n > 90)
            result = (number)NAN; // illegal instruction on OpenBSD / on Linux typically an overflow
        else {
            long long int fib[n + 1];
            fib[0] = 0;
            fib[1] = 1;
            long long int sum = fib[0] + fib[1];

            for (int i = 2; i <= n; i++) {
                fib[i] = fib[i - 1] + fib[i - 2];
                sum += fib[i];
            }

            result = (number)sum;
        }
    } else if (strncmp(function, "fib", 3) == 0) {
        long long int fib1 = 1;
        long long int fib2 = 0;
        long long int sum = 0;
        result = 0;

        if ((int)num < 0)
            result = (number)NAN;
        else if ((int)num > 92)
            result = (number)NAN; // illegal instruction on OpenBSD / on Linux typically an overflow
        else {
            for (int c = 1; c < (int)num; c++) {
                long long int fn = fib1 + fib2;

                if (c > 2)  {
                    fib2 = fib1;
                    fib1 = fn;
                }

                sum += fn;
            }

            result = (number)sum;
        }
    } else if (strncmp(function, "min", 3) == 0) {
        while (stackSize(s) > 0 && strcmp(stackTop(s), FUNCTIONSEPARATOR) != 0) {
            input = (token)stackPop(s);
            num = buildNumber(input);
            printf("%f %s", num, input);

            if (num < result)
                result = num;
        }
    } else if (strncmp(function, "max", 3) == 0) {
        while (stackSize(s) > 0 && strcmp(stackTop(s), FUNCTIONSEPARATOR) != 0) {
            input = (token)stackPop(s);
            num = buildNumber(input);

            if (num > result)
                result = num;
        }
    } else if (strncmp(function, "sum", 3) == 0) {
        while (stackSize(s) > 0 && strcmp(stackTop(s), FUNCTIONSEPARATOR) != 0) {
            input = (token)stackPop(s);
            num = buildNumber(input);
            result += num;
        }
    } else if (strncmp(function, "avg", 3) == 0 ||
               strncmp(function, "mean", 4) == 0) {
        // Result already initialized with first number
        counter = 1;

        while (stackSize(s) > 0  && strcmp(stackTop(s), FUNCTIONSEPARATOR) != 0) {
            input = (token)stackPop(s);
            num = buildNumber(input);
            result += num;
            counter++;
        }

        result /= counter;
    } else if (strncmp(function, "median", 6) == 0) {
        // needed for sorting
        Stack tmp, safe;
        // Result already initialized with first number
        counter = 1;
        stackInit(&tmp, (unsigned int)(stackSize(s) > 0 ? stackSize(s) : 1));
        stackInit(&safe, (unsigned int)(stackSize(s) > 0 ? stackSize(s) : 1));
        // add first value to the later sorted stack
        stackPush(&tmp, input);

        while (stackSize(s) > 0  && strcmp(stackTop(s), FUNCTIONSEPARATOR) != 0) {
            input = (token)stackPop(s);
            num = buildNumber(input);

            // save all numbers larger as the stack value
            while (stackSize(&tmp) > 0 && buildNumber(stackTop(&tmp)) < num) {
                stackPush(&safe, stackPop(&tmp));
            }

            // push value on the sorted stack
            stackPush(&tmp, input);

            // push all saved numbers back on the sorted stack
            while (stackSize(&safe) > 0) {
                stackPush(&tmp, stackPop(&safe));
            }

            counter++;
        }

        stackFree(&safe);
        // calculate the median index
        counter = (number)(((int)counter + 1) / 2);

        // pop all numbers until median index
        while (counter > 1) {
            stackPop(&tmp);
            counter--;
        }

        result = buildNumber(stackPop(&tmp));

        // pop the remaining sorted stack
        while (stackSize(&tmp) > 0) {
            stackPop(&tmp);
        }

        stackFree(&tmp);
    } else if (strncmp(function, "var", 3) == 0) {
        Stack tmp;
        counter = 1;
        // second stack to store values during calculation of mean
        stackInit(&tmp, (unsigned int)(stackSize(s) > 0 ? stackSize(s) : 1));
        // push first value to temporary stack
        stackPush(&tmp, input);
        number mean = result;

        while (stackSize(s) > 0  && strcmp(stackTop(s), FUNCTIONSEPARATOR) != 0) {
            input = (token)stackPop(s);
            // push value to temporary stack
            stackPush(&tmp, input);
            num = buildNumber(input);
            mean += num;
            counter++;
        }

        // calculate mean
        mean /= counter;
        result = 0;

        // calculate sum of squared differences
        while (stackSize(&tmp) > 0) {
            input = (token)stackPop(&tmp);
            num = buildNumber(input) - mean;
            result += pow(num, 2);
        }

        // determine variance
        result /= counter;
        stackFree(&tmp);
    }

    if (stackTop(s) && strcmp(stackTop(s), FUNCTIONSEPARATOR) == 0)
        stackPop(s);

    stackPush(s, num2Str(result));
    return 0;
}

static int doOp(Stack *s, token op) {
    int err = 0;
    token roperand = (token)stackPop(s);
    token loperand = (token)stackPop(s);
    number lside = buildNumber(loperand);
    number rside = buildNumber(roperand);
    number ret;
    ret = 0;

    switch (*op) {
        case '^': {
            ret = pow(lside, rside);
        }
        break;

        case '*': {
            ret = lside * rside;
        }
        break;

        case '/': {
            if ((int)rside == 0) {
                enhance(divZero);

                if ((int)lside == 0)
                    ret = (number)NAN;
                else
                    ret = (number)INFINITY;

                err = -1;
            } else
                ret = lside / rside;
        }
        break;

        case '%': {
            if ((int)rside == 0) {
                enhance(divZero);

                if ((int)lside == 0)
                    ret = (number)NAN;
                else
                    ret = (number)INFINITY;

                err = -1;
            } else {
                ret = (int)(lside / rside);
                ret = lside - (ret * rside);
            }
        }
        break;

        case '+': {
            ret = lside + rside;
        }
        break;

        case '-': {
            ret = lside - rside;
        }
        break;
    }

    stackPush(s, num2Str(ret));
    return err;
}

/*
 * Similar to fgets(), but handles automatic reallocation of the buffer.
 * Only parameter is the input stream.
 * Return value is a string. Don't forget to free it.
 */
static char *ufgets(bool in, bool eq) {
    unsigned int maxlen = 128, size = 128;
    char *buffer = (char *)malloc(maxlen);

    if (buffer != NULL) { /* NULL if malloc() fails */
        char ch = EOF;
        unsigned int pos = 0;

        /* Read input one character at a time, resizing the buffer as necessary */
        while ((ch = (char)getchar()) != EOF && ch != '\n') {
            buffer[pos++] = ch;

            if (pos == size) { /* Next character to be inserted needs more memory */
                size = pos + maxlen;
                buffer = (char *)realloc(buffer, size);
            }
        }

        if (in && ch == EOF)
            return EXIT_SUCCESS;

        buffer[pos] = '\0'; /* Null-terminate the completed string */
    }

    if (eq)
        printf("%s", buffer);

    return buffer;
}

static Symbol type(char ch) {
    Symbol result;

    switch (ch) {
        case '+':
        case '-':
            result = addop;
            break;

        case '*':
        case '/':
        case '%':
            result = multop;
            break;

        case '^':
            result = expop;
            break;

        case '(':
            result = lparen;
            break;

        case ')':
            result = rparen;
            break;

        case '.':
            result = decimal;
            break;

        case ' ':
            result = space;
            break;

        case ',':
            result = argsep;
            break;

        default:
            if (ch >= '0' && ch <= '9')
                result = digit;
            else if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
                result = text;
            else
                result = invalid;

            break;
    }

    return result;
}

static bool isFunction(token tk) {
    return (strncmp(tk, "abs", 3) == 0
            || strncmp(tk, "floor", 5) == 0
            || strncmp(tk, "ceil", 4) == 0
            || strncmp(tk, "sin", 3) == 0
            || strncmp(tk, "cos", 3) == 0
            || strncmp(tk, "tan", 3) == 0
            || strncmp(tk, "arcsin", 6) == 0
            || strncmp(tk, "arccos", 6) == 0
            || strncmp(tk, "arctan", 6) == 0
            || strncmp(tk, "asin", 4) == 0
            || strncmp(tk, "acos", 4) == 0
            || strncmp(tk, "atan", 4) == 0
            || strncmp(tk, "sqrt", 4) == 0
            || strncmp(tk, "cbrt", 4) == 0
            || strncmp(tk, "log", 3) == 0
            || strncmp(tk, "min", 3) == 0
            || strncmp(tk, "max", 3) == 0
            || strncmp(tk, "sum", 3) == 0
            || strncmp(tk, "avg", 3) == 0
            || strncmp(tk, "mean", 4) == 0
            || strncmp(tk, "median", 6) == 0
            || strncmp(tk, "var", 3) == 0
            || strncmp(tk, "exp", 3) == 0
            || strncmp(tk, "round", 5) == 0
            || strncmp(tk, "fib", 3) == 0
            || strncmp(tk, "fibs", 4) == 0);
}

static bool isSpecialValue(token tk) {
    return (strncmp(tk, "nan", 3) == 0 || strncmp(tk, "inf", 3) == 0);
}

static Symbol tokenType(token tk) {
    if (!tk)
        return invalid;

    Symbol ret = type(*tk);

    switch (ret) {
        case text:
            if (isFunction(tk))
                ret = function;
            else if (isSpecialValue(tk))
                ret = value;
            else
                ret = identifier;

            break;

        case addop:
            if (*tk == '-' && strlen(tk) > 1)
                ret = tokenType(tk + 1);

            break;

        case decimal:
        case digit:
            ret = value;
            break;

        default:
            break;
    }

    return ret;
}

static unsigned int tokenize(char *str, char *(**tokensRef)) {
    int i = 0;
    char **tokens = NULL;
    char **tmp = NULL;
    char *ptr = str;
    char ch = '\0';
    unsigned int numTokens = 0;
    char *tmpToken = malloc((prefs.maxtokenlength + 1) * sizeof(char));

    if (!tmpToken) {
        fprintf(stderr, "Malloc of temporary buffer failed\n");
        return 0;
    }

    while ((ch = *ptr++)) {
        if (type(ch) == invalid) // Stop tokenizing when we encounter an invalid character
            break;

        token newToken = NULL;
        tmpToken[0] = '\0';

        switch (type(ch)) {
            case addop: {
                // Check if this is a negative
                if (ch == '-'
                        && (numTokens == 0
                            || (tokenType(tokens[numTokens - 1]) == addop
                                || tokenType(tokens[numTokens - 1]) == multop
                                || tokenType(tokens[numTokens - 1]) == expop
                                || tokenType(tokens[numTokens - 1]) == lparen
                                || tokenType(tokens[numTokens - 1]) == argsep))) {
                    // Assemble an n-character (plus null-terminator) number token
                    {
                        unsigned int len = 1;
                        bool hasDecimal = false;
                        bool hasExponent = false;
                        bool isSpecial = isSpecialValue(ptr);

                        if (type(ch) == decimal) { // Allow numbers to start with decimal
                            //printf("Decimal\n");
                            hasDecimal = true;
                            len++;
                            tmpToken[0] = '0';
                            tmpToken[1] = '.';
                        } else { // Numbers that do not start with decimal
                            tmpToken[len - 1] = ch;
                        }

                        // Assemble rest of number
                        for (; // Don't change len
                                *ptr // There is a next character and it is not null
                                && len <= prefs.maxtokenlength
                                && (type(*ptr) == digit // The next character is a digit
                                    || ((type(*ptr) == decimal // Or the next character is a decimal
                                         && hasDecimal == 0)) // But we have not added a decimal
                                    || ((*ptr == 'E' || *ptr == 'e') // Or the next character is an exponent
                                        && hasExponent == false) // But we have not added an exponent yet
                                    || ((*ptr == '+' || *ptr == '-') && hasExponent == true) // Exponent with sign
                                    || (isSpecial && type(*ptr) == text)); //
                                ++len) {
                            if (type(*ptr) == decimal)
                                hasDecimal = true;
                            else if (*ptr == 'E' || *ptr == 'e')
                                hasExponent = true;

                            tmpToken[len] = *ptr++;
                        }

                        // Append null-terminator
                        tmpToken[len] = '\0';
                    }
                    break;
                }

                // If it's not part of a number, it's an op - fall through
            }

            // fall through
            case multop:
            case expop:
            case lparen:
            case rparen:
            case argsep:
                // Assemble a single-character (plus null-terminator) operation token
            {
                tmpToken[0] = ch;
                tmpToken[1] = '\0';
            }
            break;

            case digit:
            case decimal:
                // Assemble an n-character (plus null-terminator) number token
            {
                unsigned int len = 1;
                bool hasDecimal = false;
                bool hasExponent = false;

                if (type(ch) == decimal) { // Allow numbers to start with decimal
                    //printf("Decimal\n");
                    hasDecimal = true;
                    len++;
                    tmpToken[0] = '0';
                    tmpToken[1] = '.';
                } else { // Numbers that do not start with decimal
                    tmpToken[len - 1] = ch;
                }

                // Assemble rest of number
                for (; // Don't change len
                        *ptr // There is a next character and it is not null
                        && len <= prefs.maxtokenlength
                        && (type(*ptr) == digit // The next character is a digit
                            || ((type(*ptr) == decimal // Or the next character is a decimal
                                 && hasDecimal == 0)) // But we have not added a decimal
                            || ((*ptr == 'E' || *ptr == 'e') // Or the next character is an exponent
                                && hasExponent == false) // But we have not added an exponent yet
                            || ((*ptr == '+' || *ptr == '-') && hasExponent == true)); // Exponent with sign
                        ++len) {
                    if (type(*ptr) == decimal)
                        hasDecimal = true;
                    else if (*ptr == 'E' || *ptr == 'e')
                        hasExponent = true;

                    tmpToken[len] = *ptr++;
                }

                // Append null-terminator
                tmpToken[len] = '\0';
            }
            break;

            case text:
                // Assemble an n-character (plus null-terminator) text token and normalize to lower case.
            {
                unsigned int len = 1;
                tmpToken[0] = (char)tolower(ch);

                for (len = 1; *ptr && type(*ptr) == text && len <= prefs.maxtokenlength; ++len) {
                    tmpToken[len] = (char)tolower(*ptr++);
                }

                tmpToken[len] = '\0';
            }
            break;

            default:
                break;
        }

        size_t tokenLength = strlen(tmpToken);

        // Add to list of tokens
        if (tmpToken[0] != '\0' && tokenLength > 0) {
            numTokens++;
            newToken = malloc((tokenLength + 1) * sizeof(char));

            if (!newToken && numTokens > 0) {
                numTokens--;
                break;
            }

            strcpy(newToken, tmpToken);
            newToken[tokenLength] = '\0';
            tmp = (char **)realloc(tokens, numTokens * sizeof(char *));

            if (tmp == NULL) {
                if (tokens != NULL) {
                    for (i = 0; i < (int)numTokens - 1; i++) {
                        if (tokens[i] != NULL)
                            free(tokens[i]);
                    }

                    free(tokens);
                }

                *tokensRef = NULL;
                free(newToken);
                free(tmpToken);
                return 0;
            }

            tokens = tmp;
            tmp = NULL;
            tokens[numTokens - 1] = newToken;
        }
    }

    *tokensRef = tokens; // Send back out
    free(tmpToken);
    tmpToken = NULL;
    return numTokens;
}

static bool leftAssoc(token op) {
    bool ret = false;

    switch (tokenType(op)) {
        case addop:
        case multop:
            ret = true;
            break;

        case function:
        case expop:
            ret = false;
            break;

        default:
            break;
    }

    return ret;
}

static int precedence(token op1, token op2) {
    int ret = 0;

    if (op2 == NULL)
        ret = 1;
    else if (tokenType(op1) == tokenType(op2)) // Equal precedence
        ret = 0;
    else if (tokenType(op1) == addop
             && (tokenType(op2) == multop || tokenType(op2) == expop)) // op1 has lower precedence
        ret = -1;
    else if (tokenType(op2) == addop
             && (tokenType(op1) == multop || tokenType(op1) == expop)) // op1 has higher precedence
        ret = 1;
    else if (tokenType(op1) == multop
             && tokenType(op2) == expop) // op1 has lower precedence
        ret = -1;
    else if (tokenType(op1) == expop
             && tokenType(op2) == multop) // op1 has higher precedence
        ret = 1;
    else if (tokenType(op1) == function
             && (tokenType(op2) == addop || tokenType(op2) == multop || tokenType(op2) == expop || tokenType(op2) == lparen))
        ret = 1;
    else if ((tokenType(op1) == addop || tokenType(op1) == multop || tokenType(op1) == expop)
             && tokenType(op2) == function)
        ret = -1;

    return ret;
}

static void evalStackPush(Stack *s, token val) {
    if (prefs.display.postfix)
        printf("\t%s\n", val);

    switch (tokenType(val)) {
        case function: {
            if (doFunc(s, val) < 0)
                return;
        }
        break;

        case expop:
        case multop:
        case addop: {
            if (stackSize(s) >= 2) {
                // Pop two operands

                // Evaluate
                if (doOp(s, val) < 0)
                    return;

                // Push result
                //stackPush(s, res);
            } else {
                stackPush(s, val);
            }
        }
        break;

        case value: {
            stackPush(s, val);
        }
        break;

        default:
            break;
    }
}

static bool postfix(token *tokens, unsigned int numTokens, Stack *output) {
    Stack operators, intermediate;
    int i;
    bool err = false;
    stackInit(&operators, numTokens);
    stackInit(&intermediate, numTokens);

    for (i = 0; i < (int)numTokens; i++) {
        // From Wikipedia/Shunting-yard_algorithm:
        switch (tokenType(tokens[i])) {
            case value: {
                // If the token is a number, then add it to the output queue.
                evalStackPush(output, tokens[i]);
            }
            break;

            case function: {
                while (stackSize(&operators) > 0
                        && (tokenType(tokens[i]) != lparen)
                        && ((precedence(tokens[i], (char *)stackTop(&operators)) <= 0))) {
                    evalStackPush(output, stackPop(&operators));
                    stackPush(&intermediate, stackTop(output));
                }

                // If the token is a function token, then push it onto the stack.
                stackPush(&operators, tokens[i]);
            }
            break;

            case argsep: {
                /*
                 * If the token is a function argument separator (e.g., a comma):
                 *   Until the token at the top of the stack is a left
                 *   paren, pop operators off the stack onto the output
                 *   queue. If no left paren encountered, either separator
                 *   was misplaced or parens mismatched.
                 */
                while (stackSize(&operators) > 0
                        && tokenType((token)stackTop(&operators)) != lparen
                        && stackSize(&operators) > 1) {
                    evalStackPush(output, stackPop(&operators));
                    stackPush(&intermediate, stackTop(output));
                }
            }
            break;

            case addop:
            case multop:
            case expop: {
                /*
                 * If the token is an operator, op1, then:
                 *   while there is an operator token, op2, at the top of the stack, and
                 *           either op1 is left-associative and its precedence is less than or equal to that of op2,
                 *           or op1 is right-associative and its precedence is less than that of op2,
                 *       pop op2 off the stack, onto the output queue
                 *   push op1 onto the stack
                 */
                while (stackSize(&operators) > 0
                        && (tokenType((char *)stackTop(&operators)) == addop || tokenType((char *)stackTop(&operators)) == multop || tokenType((char *)stackTop(&operators)) == expop)
                        && ((leftAssoc(tokens[i]) && precedence(tokens[i], (char *)stackTop(&operators)) <= 0)
                            || (!leftAssoc(tokens[i]) && precedence(tokens[i], (char *)stackTop(&operators)) < 0))) {
                    evalStackPush(output, stackPop(&operators));
                    stackPush(&intermediate, stackTop(output));
                }

                stackPush(&operators, tokens[i]);
            }
            break;

            case lparen: {
                // If the token is a left paren, then push it onto the stack
                if (tokenType(stackTop(&operators)) == function)
                    stackPush(output, FUNCTIONSEPARATOR);

                stackPush(&operators, tokens[i]);
            }
            break;

            case rparen: {
                /*
                 * If the token is a right paren:
                 *   Until the token at the top of the stack is a left paren, pop operators off the stack onto the output queue
                 *   Pop the left paren from the stack, but not onto the output queue
                 *   If the stack runs out without finding a left paren, then there are mismatched parens
                 */
                while (stackSize(&operators) > 0
                        && tokenType((token)stackTop(&operators)) != lparen
                        && stackSize(&operators) > 1) {
                    evalStackPush(output, stackPop(&operators));
                    stackPush(&intermediate, stackTop(output));
                }

                if (stackSize(&operators) > 0
                        && tokenType((token)stackTop(&operators)) != lparen) {
                    err = true;
                    enhance(parenMismatch);
                }

                stackPop(&operators); // Discard lparen

                while (stackSize(&operators) > 0 && tokenType((token)stackTop(&operators)) == function) {
                    evalStackPush(output, stackPop(&operators));
                    stackPush(&intermediate, stackTop(output));
                }
            }
            break;

            default:
                break;
        }
    }

    /*
     * When there are no more tokens to read:
     *   While there are still operator tokens on the stack:
     *       If the operator token on the top of the stack is a paren, then there are mismatched parens
     *       Pop the operator onto the output queue
     */
    while (stackSize(&operators) > 0) {
        if (tokenType((token)stackTop(&operators)) == lparen) {
            enhance(parenMismatch);
            err = true;
        }

        evalStackPush(output, stackPop(&operators));
        stackPush(&intermediate, stackTop(output));
    }

    // pop result from intermediate stack
    stackPop(&intermediate);

    // free remaining intermediate results
    while (stackSize(&intermediate) > 0) {
        token s = stackPop(&intermediate);
        free(s);
    }

    if (err == true) {
        while (stackSize(&operators) > 0) {
            token s = stackPop(&operators);
            free(s);
        }
    }

    stackFree(&intermediate);
    stackFree(&operators);
    return err;
}

static unsigned int strSplit(char *str, const char split, char *(**partsRef)) {
    char **parts = NULL;
    char **tmpparts = NULL;
    char *ptr = str;
    char *part = NULL;
    char *tmppart = NULL;
    unsigned int numParts = 0;
    char ch;
    unsigned int len = 0;

    while (1) {
        ch = *ptr++;

        if ((ch == '\0' || ch == split) && part != NULL) { // End of part
            // Add null terminator
            tmppart = (char *)realloc(part, (len + 1) * sizeof(char));

            // if realloc fails, free current part and all previous parts
            if (tmppart == NULL) {
                free(part);
                part = NULL;

                for (len = 0; len < numParts; len++) {
                    if (parts[len])
                        free(parts[len]);
                }

                if (parts)
                    free(parts);

                parts = NULL;
                numParts = 0;
                break;
            }

            part = tmppart;
            part[len] = '\0';
            // Add to parts
            numParts++;

            if (parts == NULL)
                parts = (char **)malloc(sizeof(char **));
            else {
                tmpparts = (char **)realloc(parts, numParts * sizeof(char *));

                // if relloc fails, free current and previous parts
                if (tmpparts == NULL) {
                    free(part);
                    part = NULL;

                    for (len = 0; len < numParts - 1; len++) {
                        if (parts[len])
                            free(parts[len]);
                    }

                    if (parts)
                        free(parts);

                    parts = NULL;
                    numParts = 0;
                    break;
                }

                parts = tmpparts;
            }

            parts[numParts - 1] = part;
            part = NULL;
            len = 0;
        } else { // Add to part
            len++;

            if (part == NULL) {
                part = (char *)malloc(sizeof(char));
            } else {
                tmppart = (char *)realloc(part, len * sizeof(char));

                // if relloc fails, free current and previous parts
                if (tmppart == NULL) {
                    free(part);
                    part = NULL;

                    for (len = 0; len < numParts; len++) {
                        if (parts[len])
                            free(parts[len]);
                    }

                    free(parts);
                    numParts = 0;
                    parts = NULL;
                    break;
                }

                part = tmppart;
            }

            part[len - 1] = ch;
        }

        if (ch == '\0')
            break;
    }

    *partsRef = parts;
    return numParts;
}

static bool execCommand(char *str) {
    int i = 0;
    bool recognized = false;
    char **words = NULL;
    unsigned int len = strSplit(str, ' ', &words);

    if (len >= 1 && strcmp(words[0], "get") == 0) {
        if (len >= 2 && strcmp(words[1], "display") == 0) {
            if (len >= 3 && strcmp(words[2], "tokens") == 0) {
                recognized = true;
                printf("\t%s\n", (prefs.display.tokens ? "on" : "off"));
            } else if (len >= 3 && strcmp(words[2], "postfix") == 0) {
                recognized = true;
                printf("\t%s\n", (prefs.display.postfix ? "on" : "off"));
            }
        } else if (len >= 2 && strcmp(words[1], "mode") == 0) {
            recognized = true;
            printf("\t%s\n", (prefs.mode.degrees ? "degrees" : "radians"));
        } else if (len >= 2 && strcmp(words[1], "precision") == 0) {
            recognized = true;

            if (prefs.precision > 0)
                printf("\t%d\n", prefs.precision);
            else
                printf("\tauto\n");
        }
    } else if (len >= 1 && strcmp(words[0], "set") == 0) {
        if (len >= 2 && strcmp(words[1], "display") == 0) {
            if (len >= 3 && strcmp(words[2], "tokens") == 0) {
                if (len >= 4 && strcmp(words[3], "on") == 0) {
                    recognized = true;
                    prefs.display.tokens = true;
                } else if (len >= 4 && strcmp(words[3], "off") == 0) {
                    recognized = true;
                    prefs.display.tokens = false;
                }
            } else if (len >= 3 && strcmp(words[2], "postfix") == 0) {
                if (len >= 4 && strcmp(words[3], "on") == 0) {
                    recognized = true;
                    prefs.display.postfix = true;
                } else if (len >= 4 && strcmp(words[3], "off") == 0) {
                    recognized = true;
                    prefs.display.postfix = false;
                }
            }
        } else if (len >= 2 && strcmp(words[1], "mode") == 0) {
            if (len >= 3 && strcmp(words[2], "radians") == 0) {
                recognized = true;
                prefs.mode.degrees = false;
            } else if (len >= 3 && strcmp(words[2], "degrees") == 0) {
                recognized = true;
                prefs.mode.degrees = true;
            }
        } else if (len >= 2 && strcmp(words[1], "precision") == 0) {
            if (len >= 3 && strcmp(words[2], "auto") == 0) {
                recognized = true;
                prefs.precision = -1;
            } else if (len >= 3 && type(words[2][0]) == digit) {
                recognized = true;
                prefs.precision = atoi(words[2]);
            }
        }
    }

    if (words) {
        for (i = 0; i < (int)len; i++) {
            if (words[i])
                free(words[i]);
        }

        free(words);
    }

    return recognized;
}

int main(int argc, char *argv[]) {
    bool once = true;
    bool lr = true;
    bool in = false;
    bool eq = false;
    char *str = NULL;
    token *tokens = NULL;
    unsigned int numTokens = 0;
    Stack expr;
    int i;
    int ch, rflag = 0;
    prefs.precision = DEFAULTPRECISION;
    prefs.maxtokenlength = MAXTOKENLENGTH;
    char *basec, *bname;
    char tmpf[255];
    struct passwd *pw = getpwuid(getuid());
    basec = strdup(argv[0]);
    bname = basename(basec);
    snprintf(tmpf, sizeof(tmpf), "%s/.last.%s", pw->pw_dir, NAME);
#if defined(__OpenBSD__)

    if (pledge("stdio cpath rpath wpath unveil", NULL) == -1)
        err(1, "pledge");

    if (unveil(tmpf, "crw") == -1)
        err(1, "unveil failed for %s", tmpf);

    if (unveil(NULL, NULL) == -1)
        err(1, "unveil block failed");

#endif

    if ( strncmp(bname, ALTNAME, sizeof(ALTNAME)) == 0 )
        return lastResult(tmpf);

    if ( ( strncmp(bname, NAME, sizeof(NAME)) != 0 ) && ( strncmp(bname, ALTNAME, sizeof(ALTNAME)) != 0 ) ) {
        printf("Please call calculator only as %s or %s.\n", NAME, ALTNAME);
        return EXIT_FAILURE;
    }

    free(basec);

    if ( argc == 1 )
        return usage(false);

    for (char **pargv = argv + 1; *pargv != argv[argc]; pargv++) {
        if ((int) strlen(*pargv) == 6 ) {
            int ret = 1;
            ret = strncmp(*pargv, "--help", 6);

            if (ret == 0) {
                return usage(false);
            }

            ret = strncmp(*pargv, "--last", 6);

            if (ret == 0) {
                return lastResult(tmpf);
            }
        }

        if ((int) strlen(*pargv) == 9 ) {
            int ret = 1;
            ret = strncmp(*pargv, "--version", 9);

            if (ret == 0) {
                printf("%s\n", VERSION);
                return EXIT_SUCCESS;
            }

            ret = strncmp(*pargv, "--license", 9);

            if (ret == 0) {
                printf("%s", license);
                return EXIT_SUCCESS;
            }
        }
    }

    while ((ch = getopt(argc, argv, "dlsrnp:")) != -1) {
        switch (ch) {
            case 'd':
                once = false;
                break;

            case 'r':
                rflag = 1;
                break;

            case 'p':
                prefs.precision = atoi(optarg);
                break;

            case 'l':
                return lastResult(tmpf);

            case 'n':
                lr = false;
                break;

            case 's':
                eq = true;
                break;

            default:
                return usage(true);
        }
    }

    if ( argc > 1 && once ) {
        if ( strncmp(argv[argc - 1], "-", 1) == 0 ) {
            in = true;
            once = false;
        }
    }

#if defined(__OpenBSD__)

    if (!once && !in)
        if (pledge("stdio unveil", NULL) == -1)
            err(1, "pledge");

#endif

    if ( once && argc > optind ) {
        str = strdup(argv[argc - 1]);

        if (eq && !rflag)
            printf("%s", str);

        goto no_command;
    }

    if ((argc == optind && once) || (argc == optind && in)) {
        usage(true);
        return EXIT_FAILURE;
    }

    if ( in )
        str = "";
    else
        str = ufgets(in, (eq && !rflag));

    while (str != NULL && strcmp(str, "quit") != 0) {
        if (strlen(str) == 0)
            goto get_new_string;

        if (strcmp(str, "help") == 0) {
            help();
            goto get_new_string;
        }

        if (type(*str) == text) {
            // Do something with command
            if (!execCommand(str))
                goto no_command;

            free(str);
            str = NULL;
        } else {
no_command:
            //prefs.display.tokens = true;
            numTokens = tokenize(str, &tokens);
            free(str);
            str = NULL;

            if (prefs.display.tokens) {
                printf("\t%d tokens:\n", numTokens);

                for (i = 0; i < (int)numTokens; i++) {
                    printf("\t\"%s\"", tokens[i]);

                    if (tokenType(tokens[i]) == value)
                        printf(" = %f", buildNumber(tokens[i]));

                    printf("\n");
                }
            }

            // Convert to postfix
            stackInit(&expr, numTokens);

            if (prefs.display.postfix)
                printf("\tPostfix stack:\n");

            postfix(tokens, numTokens, &expr);

            if (stackSize(&expr) != 1) {
                fprintf(stderr, "Error evaluating expression\n");

                if (once)
                    return EXIT_FAILURE;
            } else {
                if (!rflag)
                    printf("\t= ");

                printf("%s\n", (char *)stackTop(&expr));

                if (once || in) {
                    if (lr) {
                        FILE *fp;
                        fp = fopen(tmpf, "w");

                        if (fp == NULL) {
                            fprintf(stderr, "Error opening %s file!\n", tmpf);
                            return EXIT_FAILURE;
                        } else {
                            fprintf(fp, "%s\n", (char *)stackTop(&expr));
                            fclose(fp);
                        }
                    }
                }

                for (i = 0; i < (int)numTokens; i++) {
                    if (tokens[i] == stackTop(&expr))
                        tokens[i] = NULL;
                }

                free(stackPop(&expr));
            }

            for (i = 0; i < (int)numTokens; i++) {
                if (tokens[i] != NULL)
                    free(tokens[i]);
            }

            free(tokens);
            tokens = NULL;
            numTokens = 0;
            stackFree(&expr);

            if (once)
                return EXIT_SUCCESS;
        }

get_new_string:
        str = ufgets(in, (eq && !rflag));
    }

    free(str);
    str = NULL;
    return EXIT_SUCCESS;
}
