// SPDX-FileCopyrightText: 2020 Antonino Catinello
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <stdbool.h>
#include "define.h"

static void help(void);
static int usage(bool err);

// Print available commands/operators/functions in dialog.
static void help(void) {
    printf("Commands in interactive mode:\n");
    printf("  help       | Show this help.\n");
    printf("  quit       | Exit this program dialog.\n");
    printf("  get/set    | Commands to read/change settings.\n");
    printf("  Settings:\n");
    printf("    display {postfix | tokens} (off/on)\t | Display the postfix stack or the result of tokenization.\n");
    printf("    mode (radians/degrees)\t\t | Evaluation mode of trigonometric functions.\n");
    printf("    precision (X/auto)\t\t\t | Set precision to X decimal places.\n\n");
    printf("Operators:\n");
    printf("  +\t addition\n");
    printf("  -\t subtraction\n");
    printf("  *\t multiplication\n");
    printf("  /\t division\n");
    printf("  ^\t exponent\n");
    printf("  %s\t modulus\n\n", "%");
    printf("Functions:\n");
    printf("  abs(...)\t absolute value\n");
    printf("  floor(...)\t floor\n");
    printf("  ceil(...)\t ceiling\n");
    printf("  sin(...)\t sine\n");
    printf("  cos(...)\t cosine\n");
    printf("  tan(...)\t tangent\n");
    printf("  asin(...)\t arcsine\n");
    printf("  acos(...)\t arccosine\n");
    printf("  atan(...)\t arctangent\n");
    printf("  sqrt(...)\t square root\n");
    printf("  cbrt(...)\t cube root\n");
    printf("  log(...)\t logarithm\n");
    printf("  exp(...)\t exponentiation (e^x)\n");
    printf("  min(...)\t minimum\n");
    printf("  max(...)\t maximum\n");
    printf("  sum(...)\t summation\n");
    printf("  mean(...)\t mean\n");
    printf("  avg(...)\t average\n");
    printf("  median(...)\t median\n");
    printf("  var(...)\t variance\n");
    printf("  round(...)\t round\n");
    printf("  fib(...)\t fibonacci\n");
    printf("  fibs(...)\t sum of fibonacci\n");
}

// Print usage from command line and more.
static int usage(bool err) {
    printf("%s - Command Line Calculator\n\n", NAME);
    printf("Usage:\n");
    printf("  %s [OPTIONS] [EQUATION]\n\n", NAME);
    printf("Options:\n");
    printf("  -d           | Interactive dialog mode.\n");
    printf("  -p N         | Set precision to X decimal places (-1 = auto = default).\n");
    printf("  -r           | Result only. Suppress prefix '=' or other output.\n");
    printf("  -s           | Show equation before result.\n");
    printf("  -n           | Do not update last result value.\n");
    printf("  -l | --last  | Shows last calc result if available. (see lr command)\n");
    printf("  --help       | Shows this help.\n");
    printf("  --version    | Print version info.\n");
    printf("  --license    | Print full license.\n\n");
    printf("Equation:\n");
    printf("  1+2          | See better examples below\n");
    printf("  -            | Reading from stdin per line until quit command or EOF.\n");
    printf("               | Similar to -d (interactive) but chainable.\n\n");
    printf("Examples:\n");
    printf("  %s -r 1+2                         | Simple calculation without result prefix.\n", NAME);
    printf("  3.00000\n\n");
    printf("  %s \"(1+2)/sqrt(4)\"                | Quoting a more complex calculation.\n", NAME);
    printf("      = 1.50000\n\n");
    printf("  %s -d                             | Going into dialog mode.\n\n", NAME);
    printf("  BITS=128; MASK=64;                  | Using shell variables.\n");
    printf("  %s -s -p0 \"2^($BITS-$MASK)\"       | Show values and result without after decimal point values.\n", NAME);
    printf("  2^(128-64)	= 18446744073709551616\n\n");
    help();
    printf("\nWebsite:\n");
    printf("  https://catinello.eu/calculator\n");
    printf("\nLicense:\n");
    printf("  %s License © %s\n", LICENSE, AUTHORS);
    printf("\nVersion:\n");
    printf("  %s\n", VERSION);
    return (int)err;
}
