// SPDX-FileCopyrightText: 2020 Antonino Catinello, 2012-2016 Brandon Mills
// SPDX-License-Identifier: MIT

typedef struct {
    void **content;
    unsigned int size;
    int top;
} Stack;

void stackInit(Stack *s, unsigned int size);
void stackPush(Stack *s, void *val);
void *stackTop(Stack *s);
void *stackPop(Stack *s);
int stackSize(Stack *s);
void stackFree(Stack *s);
