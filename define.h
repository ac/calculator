// SPDX-FileCopyrightText: 2020 Antonino Catinello
// SPDX-License-Identifier: MIT

#define AUTHORS "2020 Antonino Catinello, 2012-2016 Brandon Mills"
#define LICENSE "MIT"

#ifndef NAME
#define NAME "calc"
#endif

#ifndef ALTNAME
#define ALTNAME "lr" // last result
#endif

#ifndef VERSION
#define VERSION ""
#endif
