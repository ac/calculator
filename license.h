// SPDX-FileCopyrightText: 2020 Antonino Catinello
// SPDX-License-Identifier: MIT

#ifndef LICENSEFILE
#define LICENSEFILE
/*
 * This is a blob of the LICENSE file.
 * see license.s
*/
extern const char license[];

#endif
