#!/bin/bash
# SPDX-FileCopyrightText: 2020 Antonino Catinello
# SPDX-License-Identifier: MIT

# stack test
make stack_test
./stack_test

command -v valgrind
if (( $? == 0 )); then
  while read i; do
    valgrind --tool=memcheck --leak-check=yes --track-origins=yes -s ./calc "$i"
    if (( $? != 0 )); then
      echo "[ERROR] Last check returned an error."
      exit 1
    fi
  done <test.data

  echo "[SUCCESS] No errors found."
else
  while read i; do
    ./calc "$i"
    if (( $? != 0 )); then
      echo "[ERROR] Last check returned an error."
      exit 1
    fi
  done <test.data

  echo "[SUCCESS] No errors found."
  echo "You may want to install valgrind for memory testing."
  exit 255
fi
