# SPDX-FileCopyrightText: 2020 Antonino Catinello
# SPDX-License-Identifier: MIT

 .section ".rodata"
 .globl license
 .type license, STT_OBJECT
license:
 .incbin "LICENSE"
 .byte 0
 .size license, .-license
