// SPDX-FileCopyrightText: 2020 Antonino Catinello
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


int lastResult(const char *tmpf);

int lastResult(const char *tmpf) {
    char *line = NULL;
    size_t len = 0;

    if ( access( tmpf, R_OK ) != -1 ) {
        FILE *fp;
        fp = fopen(tmpf, "r");

        if (fp == NULL)
            return EXIT_FAILURE;

        if ( getline(&line, &len, fp) != -1 ) {
            printf("%s", line);
        } else
            printf("0\n");

        fclose(fp);

        if (line)
            free(line);
    } else
        printf("0\n");

    return EXIT_SUCCESS;
}
